# l07

[[_TOC_]]

## Description
### Mirrors
* [Github](https://github.com/OlegVlGH/l06)
### Usage
    cd <project directory>/target
    java -jar l06-<version>.jar <argument>
### Command-line arguments
    -h, --help      Display a list of terminal commands
    -v, --version   Display the program version
    -a, --about     Display developer information

## Installation
### Dependencies
* Java 11 or higher
* Maven 3.x
### Building
    cd <project directory>
    mvn install

## Author
ovk, [mail@sovsem.net](mailto:mail@sovsem.net)
