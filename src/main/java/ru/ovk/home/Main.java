package ru.ovk.home;

import ru.ovk.home.dao.ProjectDAO;
import ru.ovk.home.dao.TaskDAO;
import ru.ovk.home.entity.Task;
import ru.ovk.home.entity.Project;

import java.util.Scanner;
import static ru.ovk.home.constant.TerminalConst.*;

public class Main {
    private static final ProjectDAO projectDAO = new ProjectDAO();
    private static final TaskDAO    taskDAO    = new TaskDAO();
    private static final Scanner    scaner     = new Scanner(System.in);

    static {
        projectDAO.create("Demo Project 1");
        projectDAO.create("Demo Project 2");
        taskDAO.create("TEST Task 1");
        taskDAO.create("TEST Task 2");
    }

    public static void main(String[] args) {
        DisplayTerminalMess(args);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scaner.nextLine();
            DisplayTerminalMess(command);
        }
    }

    private static void DisplayTerminalMess(final String[] args) {
        String sCommand;
        if (args.length < 1) sCommand = "default";
        else sCommand = args[0];
        final int result = DisplayTerminalMess(sCommand);
       // System.exit(result);
    }

    private static int DisplayTerminalMess(final String sCommand) {
        if(sCommand == null || sCommand.isEmpty() ) {return 0;}

        switch (sCommand) {
            case CMD_ABOUT:      return displayAbout();
            case CMD_VERSION:    return displayVersion();
            case CMD_HELP:       return displayHelp();
            case CMD_DEFAULT:    return displayDefault();
            case CMD_EXIT:       return displayExit();

            case PROJECT_CLEAR:  return clearProject();
            case PROJECT_CREATE: return createProject();
            case PROJECT_LIST:   return listProject();
            case PROJECT_VIEW:   return viewProjectByIndex();
            case PROJECT_VIEW_BY_INDEX:    return viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:       return viewProjectById();
            case PROJECT_VIEW_BY_NAME:     return viewProjectByName();
            case PROJECT_REMOVE_BY_INDEX:  return removeProjectByIndex();
            case PROJECT_REMOVE_BY_ID:     return removeProjectById();
            case PROJECT_REMOVE_BY_NAME:   return removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:  return updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:     return updateProjectById();

            case TASK_CLEAR:            return clearTask();
            case TASK_CREATE:           return createTask();
            case TASK_LIST:             return listTask();
            case TASK_VIEW_BY_INDEX:    return viewTaskByIndex();
            case TASK_VIEW_BY_ID:       return viewTaskById();
            case TASK_VIEW_BY_NAME:     return viewTaskByName();
            case TASK_REMOVE_BY_INDEX:  return removeTaskByIndex();
            case TASK_REMOVE_BY_ID:     return removeTaskById();
            case TASK_REMOVE_BY_NAME:   return removeTaskByName();
            case TASK_UPDATE_BY_INDEX:  return updateTaskByIndex();
            case TASK_UPDATE_BY_ID:     return updateTaskById();

            default:             return displayErr();
        }
    }

    private static int displayVersion() {
        System.out.println(TELL_VERSION);
        return 0;
    }

    private static int displayAbout() {
        System.out.println(TELL_ABOUT);
        return 0;
    }

    private static int displayHelp() {
        System.out.println("\"project-create\"  create new project");
        System.out.println("\"project-list\"    list projects");
        System.out.println("\"project-clear\"   clear projects");
        System.out.println("\"project-view-by-index\"       view project by index");
        System.out.println("\"project-view-by-id\"          view project by id");
        System.out.println("\"project-view-by-name\"        view project by name");

        System.out.println("\"task-create\"         create new task");
        System.out.println("\"task-list\"           list tasks");
        System.out.println("\"task-clear\"          clear tasks");
        System.out.println("\"task-view-by-index\"  view task by index");
        System.out.println("\"task-view-by-id\"     view task by id");
        System.out.println("\"task-view-by-name\"   view task by name");
        System.out.println("\"task-remove-by-index\"  task remove by index");
        System.out.println("\"task-remove-by-id\"     task remove by id");
        System.out.println("\"task-remove-by-name\"   task remove by name");

        System.out.println("\"exit\"            exit from programm");

        return 0;
    }

    private static int displayDefault() {
        System.out.println("Welcome");
        return 0;
    }

    private static int displayErr() {
        System.out.println(TELL_ERROR);
        return -1;
    }

    private static int displayExit() {
        System.out.println(TELL_EXIT);
        return 0;
    }

    private static int createProject() {
        System.out.println("[Create project]");
        System.out.println("inpunt project name");
        final String name = scaner.nextLine();
        projectDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearProject() {
        System.out.println("[Clear project]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listProject() {
        System.out.println("[List project]");
        int index=1;
        for(final Project project: projectDAO.findAll()){
         System.out.println(index + ". " + project.getId() + ": " + project.getName());
         index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    private static void ViewProject(final Project project) {
        if(project==null) return;
        System.out.println("[View Project]");
        System.out.println( "ID: "   + project.getId());
        System.out.println( "NAME: " + project.getName());
        System.out.println( "DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    private static int viewProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final int index  = scaner.nextInt() - 1;
        final Project project = projectDAO.findByIndex(index);
        ViewProject(project);
        return 0;
    }
    private static int viewProjectById() {
        System.out.println("ENTER PROJECT ID");
        final Long id  =  scaner.nextLong();
        final Project project = projectDAO.findById(id);
        ViewProject(project);
        return 0;
    }
    private static int viewProjectByName() {
        System.out.println("ENTER Project NAME");
        final String name  =  scaner.nextLine();
        final Project project = projectDAO.findByName(name);
        ViewProject(project);
        return 0;
    }

    private static int removeProjectByIndex() {
        System.out.println("REMOVE Project BY INDEX");
        System.out.println("ENTER Project INDEX");
        final int index  = scaner.nextInt() - 1;
        final Project project  = projectDAO.removeByIndex(index);
        if(project==null) System.out.println("[FAIL]");
        else ViewProject(project);
        return 0;
    }

    private static int removeProjectById() {
        System.out.println("REMOVE Project BY ID");
        System.out.println("ENTER Project ID");
        final Long id  =  scaner.nextLong();
        final Project project = projectDAO.removeById(id);
        if( project==null) System.out.println("[FAIL]");
        else ViewProject(project);
        return 0;
    }
    private static int removeProjectByName() {
        System.out.println("ENTER Project NAME");
        final String name  =  scaner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if(project==null) System.out.println("[FAIL]");
        else ViewProject(project);
        return 0;
    }
    private static int updateProjectById() {
        System.out.println("[UPDATE Project]");
        System.out.println("PLEASE, ENTER Project ID");
        final long id = Long.parseLong(scaner.nextLine());
        final Project project = projectDAO.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER Project NAME");
        final String name = scaner.nextLine();
        System.out.println("PLEASE, ENTER Project DESCRIPTION");
        final String description = scaner.nextLine();
        projectDAO.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateProjectByIndex() {
        System.out.println("[UPDATE_PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scaner.nextLine()) -1;
        final Project project = projectDAO.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("INPUT PROJECT NAME");
        final String name = scaner.nextLine();
        System.out.println("INPUT PROJECT DESCRIPTION:");
        final String description = scaner.nextLine();
        projectDAO.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

//======================================================================================================================

    private static int createTask() {
        System.out.println("[Create Task]");
        System.out.println("inpunt task name");
        final String name = scaner.nextLine();
        taskDAO.create(name);

        System.out.println("[OK]");
        return 0;
    }

    private static int clearTask() {
        System.out.println("[Clear Task]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listTask() {
        System.out.println("[List Task]");
        int index=1;
        for(final Task task: taskDAO.findAll()){
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    private static void ViewTask(final Task task) {
        if(task==null) return;
        System.out.println("[View Tasks]");
        System.out.println( "ID: "   + task.getId());
        System.out.println( "NAME: " + task.getName());
        System.out.println( "DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    private static int viewTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final int index  = scaner.nextInt() - 1;
        final Task task = taskDAO.findByIndex(index);
        ViewTask(task);
        return 0;
    }
    private static int viewTaskById() {
        System.out.println("ENTER TASK ID");
        final Long id  =  scaner.nextLong();
        final Task task = taskDAO.findById(id);
        ViewTask(task);
        return 0;
    }
    private static int viewTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String name  =  scaner.nextLine();
        final Task task = taskDAO.findByName(name);
        ViewTask(task);
        return 0;
    }

    private static int removeTaskByIndex() {
        System.out.println("REMOVE TASK BY INDEX");
        System.out.println("ENTER TASK INDEX");
        final int index  = scaner.nextInt() - 1;
        final Task task  = taskDAO.removeByIndex(index);
        if( task==null) System.out.println("[FAIL]");
        else ViewTask(task);
        return 0;
    }

    private static int removeTaskById() {
        System.out.println("REMOVE TASK BY ID");
        System.out.println("ENTER TASK ID");
        final Long id  =  scaner.nextLong();
        final Task task = taskDAO.removeById(id);
        if( task==null) System.out.println("[FAIL]");
        else ViewTask(task);
        return 0;
    }
    private static int removeTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String name  =  scaner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if( task==null) System.out.println("[FAIL]");
        else ViewTask(task);
        return 0;
    }

    private static int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("PLEASE ENTER TASK INDEX");
        final int index = Integer.parseInt(scaner.nextLine())-1;
        final Task task = taskDAO.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE ENTER TASK NAME");
        final String name = scaner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION");
        final String description = scaner.nextLine();
        taskDAO.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("PLEASE, ENTER TASK ID");
        final long id = Long.parseLong(scaner.nextLine());
        final Task task = taskDAO.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME");
        final String name = scaner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION");
        final String description = scaner.nextLine();
        taskDAO.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }


}